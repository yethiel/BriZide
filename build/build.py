import os, shutil
import datetime

now = datetime.datetime.now()

platforms = ["linux64", "osx", "windows32", "windows64", "linux32"]

for platform in platforms:
    # Skips files
    if not os.path.isdir(platform):
        continue
    print(f"Building {platform}")

    # Uses the date as the version number
    date = datetime.datetime.today().strftime('%y.%m%d')
    
    # Writes a file containing the version number to the game dir
    with open(f"{platform}/version", "w") as f:
        f.write(date)

    # Applies files from patch folder
    for f in os.listdir("patch"):
        shutil.copytree(os.path.join("patch", f), os.path.join(platform, f))
    
    # moves all files into the app
    if platform == "osx":
        exclude = ["BriZide.app", "readme.md"]
        move = [f for f in os.listdir("osx") if f not in exclude]
        for f in move:
            shutil.move(os.path.join("osx", f), os.path.join("osx", "BriZide.app", "Contents", "Resources"))

    # Creates an archive and deletes the folder
    shutil.make_archive(f"BriZide_{platform}_{date}", 'zip', platform)
    # shutil.rmtree(platform)
