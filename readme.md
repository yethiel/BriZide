# BriZide

BriZide is a racing game set in the distant future.
Race anti-gravity ships around levels in a large cubic space station.

[**itch.io**](https://yethiel.itch.io/brizide)

[**Manual and Documentation**](https://yethiel.gitlab.io/BriZide)


![](screenshot.jpg)


## Download and Install

You can download the game on [itch.io](https://yethiel.itch.io/brizide).  
Builds for GNU/Linux, macOS and Windows are available [**here**](https://files.re-volt.io/brizide/).

### Development version

Alternatively, you can clone this repo and run the game with Blender:

```
git clone https://gitlab.com/Yethiel/BriZide.git
cd BriZide
blenderplayer brizide.blend
```
