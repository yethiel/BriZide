"""
This file contains code for creating and manipulating
a large cube of variable size (track base)
"""

from bge import logic
from modules import helpers
from mathutils import Matrix


def main():
    """
    A cube size of 10 should result in a driveable area
    of 10x10 tiles per side of the cube.
    """
    sce = helpers.get_scene("Scene")
    own = logic.getCurrentController().owner

    settings = logic.settings
    level = logic.game.get_level()

    # size of the tile objects the cube is made out of (in Blender units)
    tile_size = 32
    cube_size = level.get_cube_size()

    if cube_size > 0:
        big_cube = sce.addObject("CubeCollision", own, 0)
        big_cube.worldPosition = [(cube_size*tile_size)/2 - 16, (cube_size*tile_size)/2 - 16, (cube_size*tile_size)/2 - 16]
        big_cube.worldScale = [cube_size, cube_size, cube_size]

    if logic.settings["Video"]["detailed_cube"] == "False":
        simple_cube = sce.addObject("CubeSimple", own, 0)
        simple_cube.worldPosition = [(cube_size*tile_size)/2 - 16, (cube_size*tile_size)/2 - 16, (cube_size*tile_size)/2 - 16]
        simple_cube.worldScale = [cube_size, cube_size, cube_size]
        if cube_size > 0:
            simple_cube.meshes[0].transformUV(-1, Matrix.Identity(4)*cube_size, 0)
        
    elif logic.settings["Video"]["detailed_cube"] == "True":
        nice_cube = sce.addObject("CubeNice", own, 0)
        nice_cube.worldPosition = [(cube_size*tile_size)/2 - 16, (cube_size*tile_size)/2 - 16, (cube_size*tile_size)/2 - 16]
        nice_cube.worldScale = [cube_size, cube_size, cube_size]
        if cube_size > 0:
            nice_cube.meshes[0].transformUV(-1, Matrix.Identity(4)*cube_size*2, 0)

    # tell the components object that loading of this component is done.
    logic.components.mark_loaded("cube")


def clear():
    sce = helpers.get_scene("Scene")

    # resets mapping of the simple cube
    if logic.settings["Video"]["detailed_cube"] == "False":
        level = logic.game.get_level()
        cube_size = level.get_cube_size()
        simple_cube = sce.objects["CubeSimple"]
        if cube_size != 0:
            simple_cube.meshes[0].transformUV(-1, Matrix.Identity(4)*(1/cube_size), 0)
    elif logic.settings["Video"]["detailed_cube"] == "True":
        level = logic.game.get_level()
        cube_size = level.get_cube_size()
        simple_cube = sce.objects["CubeNice"]
        if cube_size != 0:
            simple_cube.meshes[0].transformUV(-1, Matrix.Identity(4)*(1/cube_size/2), 0)
    for obj in sce.objects:
        if "Cube" in obj.name:
            obj.endObject()
