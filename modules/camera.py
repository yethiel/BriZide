import bge
import mathutils
import math
from modules.helpers import clamp, get_scene
logic = bge.logic


def ship():
    own = logic.getCurrentController().owner
    ship = logic.getCurrentScene().objects["Ship"]
    own.fov = clamp(100 + ship.localLinearVelocity.length * 0.2, 100.0, 160.0)
    own.timeOffset = clamp(12 - ship.localLinearVelocity.length/60 * 3, 0.2, 15.0)


def skybox():
    sce = get_scene("Scene")
    own = logic.getCurrentController().owner
    cam = sce.active_camera

    if cam:
        own.worldOrientation = cam.worldOrientation
        own.fov = cam.fov
