import os

from bge import logic
from math import sin
from modules import global_constants as G


def main():

    own = logic.getCurrentController().owner
    sce = logic.getCurrentScene()

    obj_logo = sce.objects["Logo"]
    delay = 0.5

    if os.path.isfile(os.path.join(G.PATH_GAME, "config.ini")):
        logic.startGame("//main.blend")
    else:
        if not own["splash_shown"] and own["Timer"] > delay:
            own["splash_shown"] = True
            obj_info = sce.addObject("splash_info", logic.getCurrentController().owner, 0)
        if own["Timer"] > delay and own["Timer"] < 2:
            if "splash_info" in sce.objects:
                obj_info = sce.objects["splash_info"]
                obj_info.localScale[0] = 0.1+sin(own["Timer"]-delay)
                obj_info.localScale[1] = 0.1+sin(own["Timer"]-delay)
            obj_logo.worldPosition[1] += sin(own["Timer"]-delay) * 0.5
