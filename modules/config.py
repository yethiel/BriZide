from bge import logic, render
import configparser
import os.path
from modules import global_constants as G

own = logic.getCurrentController().owner

def load():
    config = configparser.ConfigParser()
    config["Game"] = {
        "leveldir" : "A01_training",
        "Mode" : "time_trial",
    }
    config["Player"] = {
        "name": "Player 0",
        "Ship": "black_coffee",
        "camera": 1
    }
    config["Audio"] = {
        "Music" : 80,
        "Master" : 60,
        "Effects" : 100,
        "startup_sound": False,
    }
    config["Video"] = {
        "fullscreen" : "False",
        "width" : 1280,
        "height" : 720,
        "detailed_cube" : "False",
        "lights" : "True",
        "extra_textures" : "True",
        "bloom" : "False",
        "blur" : "False",
        "motion_blur" : 0.0,
        "num_particles" : 30
    }

    config["Dev"] = {
        "debug" : "False",
    }

    config["Controls_Player1"] = {
        "ship_thrust" : "UPARROWKEY",
        "ship_thrust_reverse" : "DOWNARROWKEY",
        "ship_steer_left" : "LEFTARROWKEY",
        "ship_steer_right" : "RIGHTARROWKEY",
        "ship_boost" : "WKEY",
        "ship_deactivate_stabilizer" : "SKEY",
        "ship_pause" : "ESCKEY",
    }
    # Checks if the config file is there. If so, load it.
    if os.path.isfile(G.PATH_CONFIG_FILE):
        config.read(G.PATH_CONFIG_FILE)
        if G.DEBUG: print(own, "Successfully loaded config file.")
    else:
        # creates an ini with the default configuration
        # creates a new config file and write to it
        with open(G.PATH_CONFIG_FILE, 'w') as configfile:
            config.write(configfile)
        if G.DEBUG: print("Could not find config file. Created a file with defaults.")
    return config


def save():
    # print()
    logic.settings["Video"]["width"] = str(render.getWindowWidth())
    logic.settings["Video"]["height"] = str(render.getWindowHeight())
    config = logic.settings

    with open(G.PATH_CONFIG_FILE, 'w') as configfile:
        config.write(configfile)
        if G.DEBUG: print("Settings saved")


def setting_toggled(str):
    if str == "False":
        return "True"
    else:
        return "False"
