from bge import logic
from math import sin

def ease(t):
    sqt = t*t
    return sqt / (2.0 * (sqt - t) + 1.0);

def float():
    own = logic.getCurrentController().owner
    own.applyMovement((0, 0, sin(logic.uim.go["timer"]*1.5) * 0.05), True)
    own.applyRotation(((sin(logic.uim.go["timer"]*1.7) * 0.007), (sin(logic.uim.go["timer"]*1.5) * 0.008), 0), True)
