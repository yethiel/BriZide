import os
import json
from bge import logic
from modules import helpers, global_constants as G


def save_time(mode, time, date, recording_path):
    player = logic.settings["Player"]["name"]
    level = logic.game.level_name
    ship = logic.settings["Player"]["ship"]
    save_path = os.path.join(G.PATH_PROFILES, player, mode, "{}.times".format(level))
    if os.path.isfile(save_path):
        with open(save_path, "r") as f:
            times = json.load(f)
    else:
        times = {
            "level": level,
            "version": G.VERSION,
            "runs": {}
        }

    identifier = "{}:{}:{}:{}:{}".format(helpers.time_string(time), player, level, ship, date)

    times["runs"][identifier] = {
        "player": player,
        "ship": ship,
        "time": time,
        "date": date,
        "recording": recording_path
    }

    with open(save_path, "w") as f:
        json.dump(times, f)
        
    return identifier


def load_times(mode, level):
    runs = {}
    for player in os.listdir(G.PATH_PROFILES):
        player_path = os.path.join(G.PATH_PROFILES, player)
        if not os.path.isdir(player_path):
            continue
        score_file = os.path.join(
            G.PATH_PROFILES,
            player,
            mode,
            "{}.times".format(logic.game.level_name)
        )

        if os.path.isfile(score_file):
            with open(score_file, "r") as f:
                times = json.load(f)
                for run in times["runs"]:
                    runs[run] = times["runs"][run]
        elif G.DEBUG:
            print(
                "Times file does not exist for {} ({})".format(logic.game.level_name, score_file)
            )
    return runs


def get_best_time(mode, level):
    times = load_times(mode, level)
    best_time = {"player": "", "time": 9999.0}

    # Gets the best time
    for time in times:
        if times[time]["time"] < best_time["time"]:
            best_time = times[time]

    return best_time

