# Game Modes

Game modes are placed in the `modes` folder.

For example, the Time Trial game mode is structured like this:

- `time_trial`
    - `module.py`: Code for the game mode
    - `time_trial.blend`: Blender file that gets loaded into the scene

This structure is just a suggestion. As long as you place a .blend file with the same name as the folder name the game will be able to load it as a game mode.

## .blend File

The Blend file contains a simple controller object that has some logic set up.  
When you open an existing game mode you'll find the logic bricks layed out like this:

**Always sensor**: The first sensor runs the Python module with the path `modes.time_trial.module.init`.

**Always sensors in pulse mode**: There is one Always sensor in pulse mode which runs a function from the module all the time: `modes.time_trial.module.main`

Additional sensors can be added. For example, the Time Trial mode uses another sensor for the `record` function.

## Python Module

This is where the logic of the game mode goes. The most minimal mode looks like this:

```python
from bge import logic  # Blender API
from modules import btk  # for the UI
from modules.game_mode import Game_Mode  # Base game mode class

# Which components to load from the `components` folder
required_components = ["blocks", "level", "cube", "ship"]


class Free_Mode(Game_Mode):
    def __init__(self, game_obj):
        # initiates the game mode, name needs to match folder name
        super().__init__(required_components, game_obj, "free")
        self.init = False

    def setup(self):
        """ runs after loading is done """
        super().setup()
        logic.game.set_music_dir("ambient")  # music directory
        logic.uim.set_focus("ship")  # makes the ship controllable
        self.init = True

    def run(self):
        """ runs every logic tick """
        super().run()  # handles loading of components
        if not self.init:
            return
        # your code here


def init():
    """ Runs immediately after the scene loaded """
    logic.free_mode = Free_Mode(logic.getCurrentController().owner)

def main():
    logic.free_mode.run()

```

Copy this to a file and replace `Free_Mode` and `free` with your game mode's name. `free` needs to match the folder name and `Free_Mode` can be anything you like.

Now you're free to do everything you do with the Blender Game Engine. Check the Time Trial mode to see how the ship is accessed.

> This is currently still rather messy and will probably remain like that. The successor to this game will handle this more neatly and expose a simpler API for the core game components.