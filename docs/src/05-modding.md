# Modding

Almost every aspect of this game can be modified.
Game modes, levels, ships and assets can all be found in the game's folder and easily modified with free software (Blender, Gimp, text editors, ...).


[**Levels**](./05-0-modding-levels.html) can be modified and built from scratch with the Blender add-on.

[**Ships**](05-1-modding-ships.html) can be placed in the `ships` folder. They need a .blend file containing a mesh with the same name as the folder and a .inf file with some information about speed and handling. Copy an existing ship and try modifying it.

A [**custom game mode**](05-2-modding-game-modes.html) can be added with just a few lines of code. Copy any game mode folder and start modifying it. You will be able to select it in the _Game Mode_ menu.

You can also modify [other parts of the game](./05-3-modding-misc.html): Add music, modify the game's textures, sounds and the font.