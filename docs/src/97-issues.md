# Known Issues

- Fullscreen mode always runs at the desktop resolution, not the one specified in the settings
- In some desktop environments the game window jumps back when going to menu if it has been moved
- There is no controller support (I have to get a controller at some point)
- The font is pixelated at some aspect ratios
- The game has memory leaks due to Blender Game Engine (the engine the game is built with). Sadly development stopped so it's unlikely that this is going to improve. If you're experiencing problems, restart the game.
