# Menu

When you start the game for the first time, the *Enter Name* option will be selected. Press `Enter`, type your name and then press `Enter` again to confirm.

Navigate the menu with the arrow keys and `Enter`.

**Start Game**: Starts the game with all the settings you've made.

**Select Level**: Prompts you with a list of available levels. As you scroll through them, the best time information is updated in realtime.

**Select Ship**: Lets you select a race ship.

**Select Game Mode**: Select which game mode you want to play in.

**Show Replays**: Check out previous best times and watch recordings of all your runs.

**Options**: Game options.

**Quit**: Exits the game.
