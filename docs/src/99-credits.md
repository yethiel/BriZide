# Credits

## Soundtrack

ZetaSphere (race music)  
Qvarcos (ambient music)

## Sounds

URV (announcer)  
ZetaSphere (turbo sound)

## Testing

Tutenchamun  
URV  
Angerstoner  
VaidX47  
Phillip  
ZR  
Ttilman  
Paula

## Level Design

Tutenchamun (ocram, jump, vulcanid)

## Modeling

z3r0l33t (ship: black_coffee)

## Programming, Graphics

Yethiel

## Thanks to

All contributors and testers  
7shots (for the good coffee and the nice place to work)

## Inspiration

Takahiro Nakatani (creator of [AceSpeeder](http://www.raingraph.com/artist))

TrackMania

WipeOut

## Tools

Blender, GIMP, SunVox, Gedit
