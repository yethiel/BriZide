# Introduction

 <iframe src="https://itch.io/embed/393856?linkback=true&amp;bg_color=1e1e1e&amp;fg_color=e3e3e3&amp;link_color=d6a600&amp;border_color=4c4c4c" width="100%" height="167" frameborder="0"></iframe>

 Greetings!

 Here you can find out how to [configure](./02-configuration.html), [play](./04-gameplay.html) and [modify](./05-modding.html) BriZide. Check the sidebar/menu for more.

 ## Links

 [GitLab Repository](https://gitlab.com/yethiel/BriZide)

 [itch.io](https://yethiel.itch.io/brizide)

 [Discord Server](https://discord.gg/ZAadajj)

 ---

> Project 'Bright Side' was initiated by the Global Space Program in the year 2353. Its aim was to relocate humanity to colonize planets far from home due to earth's long exhausted resources. A large cubic space station was built, equipped with everything to keep the population inside occupied and save. For many centuries, the inhabitants were able to maintain a rather regular life until situations in the cube changed. Resources have gone down to a minimum and the government was overthrown. Illegal races are probably the only thing left to maintain some sort of social climate.
