# Installation

Download the .zip archive and extract it to a folder, for example

`~/BriZide`, `~/Games/Brizide`, `C:\\Users\\You\\BriZide`, `D:\\Games\\BriZide`, `/Users/you/Applications/BriZide`, ...

# Running the Game

## Windows
Double click `BriZide.exe`.

## Linux
Double click `BriZide` or open a Terminal in the game folder and type `./BriZide`.

## macOS

> Experimental

To run the app normally, you will have to [disable the gatekeeper function](https://www.tekrevue.com/tip/gatekeeper-macos-sierra/).

Move BriZide to your Programs folder. Right-click `BriZide` and select _Open_.

If this does not work (black window), open a Terminal and navigate into the app and execute blenderplayer manually:

```sh
cd /Programs/BriZide.app/Contents/MacOS
blenderplayer ../Resources/brizide.blend
```

I'd be glad about reports as I don't have a Mac to test the game.
