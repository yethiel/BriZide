# Levels

> This add-on is still experimental

Level files can be found in the `levels` folder. Every level is a `.json` file which can be edited with a regular text editor.

## Creating Levels with Blender

BriZide has a level editor add-on for Blender 2.79. You can download it [here](https://gitlab.com/yethiel/brizide-addon/-/archive/master/brizide-addon-master.zip).

To install the addon, you have to copy the folder in the .zip file to your Blender folder:

### Installation on GNU/Linux

Create the folder `~/.config/blender/2.79/scripts/addons` or enter `mkdir -p ~/.config/blender/2.79/scripts/addons` in your terminal.

Copy the `brizide-addon-master` folder into it.

### Installation on Windows

Locate your Blender folder, navigate to `2.79\scripts\addons` and copy the `brizide-addon-master` folder into it.

### Installation on macOS

Navigate to `/Applications/Blender.app/Contents/Resources/2.78/scripts/addons` and copy the `brizide-addon-master]` folder into it.

### Activating the Add-On

Open Blender and press `CTRL` + `ALT` + `U` to open the user preferences. Open the Add-Ons tab and search for `BriZide` and activate the add-on by checking the box next to it.


### Setup

You can find the add-on's ui in the left toolbar of the 3D view:

![add-on location](./img/add-on01.png)

Enter the game path then optionally press `CTRL` + `U` to save (otherwise you'll have to enter the path ever time you start Blender).

![add-on save](./img/add-on02.png)

### Creating a new Level

1. Change the cube size
2. Click on *Get all Blocks*. Only click this once.
3. Move the blocks around. To duplicate them, use `ALT` + `D`.

### Editing an existing level

1. Select a level from the list
2. Click on Load
3. Optionally add new blocks, move some around
4. Click on Save (optionally enter a different name, make sure it ends with `.json`)

> Note: Only blocks from the game will be saved to the level file. No other objects will be saved.
