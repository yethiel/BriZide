# Summary

[Introduction](./00-introduction.md)

- [Installation](./01-installation.md)
- [Configuration](./02-configuration.md)
- [Menu](./03-menu.md)
- [Gameplay and Controls](./04-gameplay.md)
- [Modding](./05-modding.md)
    - [Levels](./05-0-modding-levels.md)
    - [Ships](./05-1-modding-ships.md)
    - [Game Modes](./05-2-modding-game-modes.md)
    - [Misc.](./05-3-modding-misc.md)

[Known Issues](./97-issues.md)
[About the Project](./98-project.md)
[Credits](./99-credits.md)
