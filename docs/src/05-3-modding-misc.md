# Misc.

Feel free to explore the game's files and modify anything you like!

## Music

You can add your own music to the game by copying it into the music folders.

For example, copy files into `music/racing` and they will be selected randomly when playing the time-trial mode.
