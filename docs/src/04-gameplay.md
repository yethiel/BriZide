# Gameplay

## Default Controls

Controls can be set in the [config.ini](./02-configuration.html#configini) file.

Select options in the main menu with the arrow keys. Press `ENTER` to select a menu entry.

+ `Arrow keys`: Move ship, navigate menus
+ `W`: Boost
+ `S`: Slide / go sideways (turns stabilizer off)
+ `1` - `3`: Change camera
+ `ESC`: Show menu
+ `F8`: Take a screenshot (check the `screenshots` folder)
+ Time Trial
    + `BACKSPACE`: Start over


## Time Trial
Race around the cube and **collect all checkpoints** in any order. The bar in the top left shows you how many checkpoints are left. The bar in the bottom left shows you how much boost you have. Go sideways to generate boost.
Be quicker than the previous record to set a new highscore.
