import os, json, datetime
from bge import logic, events, render
from modules import btk, cube, components, sound, times, helpers, global_constants as G
from modules.game_mode import Game_Mode
from modules.helpers import time_string, get_scene
from random import randint

required_components = ["blocks", "level", "cube", "ship"]

trigger_distance = 32 # distance for a checkpoint to be triggered

class Time_Trial_Mode(Game_Mode):
    def __init__(self, game_obj):
        # initiates the game mode, name needs to match folder name
        super().__init__(required_components, game_obj, "time_trial")
        self.cp_data = []
        self.cp_count = 0
        self.cp_progress = {"0": 0}
        self.best_times = []
        self.best_time = {"player": "", "time": 9999.0}
        self.split_times = []
        self.final_time = 0.0
        # adds additional menu entries to the pause menu
        game = logic.game
        index_current_level = game.level_list.index(game.level_name)
        if index_current_level+1 < len(game.level_list):
            next_level_string = "Next Level: {}".format(game.level_list[index_current_level+1])
        else:
            next_level_string = "Start over with first level: {}".format(game.level_list[0])
        self.menu_texts = ["Start over [ backspace ]", next_level_string]#, "Restart Mode"]
        self.menu_actions = [self.start_over, self.next_level]#, self.restart]

        self.init = False

    def setup(self):
        """ Runs after loading is done """
        super().setup()  # generates the UI layout and menu
        sce = logic.getCurrentScene()
        logic.game.set_music_dir("racing")
        self.setup_blocks()
        self.setup_checkpoints()
        self.get_times()

        layout = logic.ui["time_trial"]

        self.label_countdown = btk.Label(layout, text="", position=[7.5, 7.5, 0], size=1.0)

        self.label_time = btk.Label(layout, text="", position=[0.5, 7.5, 0], size=0.6, update=update_label_time)
        self.label_best = btk.Label(layout, text="", position=[0.52, 7.2, 0], size=0.2)
        self.label_split = btk.Label(layout, text="", position=[7.2, 6.9, 0], size=0.3, update=update_label_split)
        
        update_label_best(self.label_best)
        btk.Label(layout, text="", position=[0.5, 8.5, 0], size=0.4, update=update_label_checkpoints)

        btk.Label(layout, text="", position=[12, 0.5, 0], size=0.6, update=update_label_speed)
        # btk.Label(layout, text="phs", position=[12.05, 0.95, 0], size=0.25)

        boost_bar = btk.ProgressBar(
            layout,
            title="boost",
            position=[0.5, 0.5, 0],
            min_scale=[0, .5, 1],
            max_scale=[4, .5, 1],
            update=update_boost_bar
        )
        boost_bar.set_color([1, .743, 0.0, 0.75])

        checkpoint_bar = btk.ProgressBar(
            layout,
            title="checkpoints_bar",
            position=[0.5, 8.45, -0.1],
            min_scale=[0, .4, 1],
            max_scale=[15, .4, 1],
            update=update_checkpoint_bar
        )
        checkpoint_bar.set_color([1, .743, 0.0, 0.75])

        logic.game.ships[0].current_boost = 500

        self.init = True

    def run(self):
        """ runs every logic tick """

        super().run()  # handles loading of components

        if not self.init:
            return
        self.countdown()
        self.checkpoints()

        if helpers.keystat("BACKSPACEKEY", "JUST_RELEASED"):
            self.start_over(None)

    def start_over(self, widget):
        """ Callback for Start Over menu entry """
        get_scene("Scene").resume()

        self.mode_done = False
        logic.recording = []
        logic.ui[self.name].get_element("pause_menu").unfocus()
        logic.ui[self.name].get_element("pause_menu").hide()
        self.setup_checkpoints()
        self.restore_blocks()
        self.get_times()
        self.restore_camera()
        update_label_best(self.label_best)
        self.label_time.hide()
        logic.uim.set_focus("time_trial")
        ship = helpers.get_scene("Scene").objects["Ship"]
        ship.worldPosition = logic.game.level.get_start_pos()
        ship.worldOrientation = logic.game.level.get_start_orientation()
        ship.linearVelocity = [0, 0, 0]
        self.final_time = 0.0
        self.split_times = []
        self.go["countdown"] = 4
        self.go["CountdownTimer"] = 0.0
        logic.game.ships[0].gravity = 0 if logic.game.get_level().cube_size == 0 else 150
        logic.game.ships[0].current_boost = 500

    def next_level(self, widget):
        get_scene("Scene").resume()
        game = logic.game
        self.mode_done = False
        index_current_level = game.level_list.index(game.level_name)
        if index_current_level + 1 < len(game.level_list):
            if index_current_level + 2 < len(game.level_list):
                widget.text = "Next level: {}".format(game.level_list[index_current_level+2])
            else:
                widget.text = "Start over with first level: {}".format(game.level_list[0])
            next_level = game.level_list[index_current_level+1]
        else:
            next_level = game.level_list[0]
            widget.text = "Next level: {}".format(game.level_list[1])
        game.level.clear()
        cube.clear()
        game.set_level(next_level)
        logic.game.save_settings()
        game.level.set_identifier(next_level)
        game.level.load()
        if G.DEBUG: game.level.print_info()
        game.level.place()
        cube.main()
        self.setup_blocks()
        self.start_over(None)

    def setup_checkpoints(self):
        self.cp_count = 0
        self.cp_progress = {"0": 0}
        self.cp_data = []
        sce = helpers.get_scene("Scene")
        for obj in sce.objects:
            if "Block_Checkpoint" in obj.name and not "end" in obj:
                obj["0"] = False
                obj.color = [1.0, 1.0, 1.0, 1]
                obj.worldScale = [1.0, 1.0, 1.0]
                self.cp_data.append(obj)
        self.cp_count = len(self.cp_data)

    def setup_blocks(self):
        """
        Sets up special blocks
        """
        sce = helpers.get_scene("Scene")
        for obj in sce.objects:
            if "Block_TrafficCone" in obj.name:
                obj["save_worldPosition"] = obj.worldPosition.copy()
                obj["save_worldOrientation"] = obj.worldOrientation.copy()

    def restore_blocks(self):
        sce = helpers.get_scene("Scene")
        if G.DEBUG: print("restoring blocks")
        for obj in sce.objects:
            if "Block_TrafficCone" in obj.name and not "end" in obj:
                obj.worldPosition = obj["save_worldPosition"] 
                obj.worldOrientation = obj["save_worldOrientation"] 
                obj.linearVelocity = [0, 0, 0]
                obj.angularVelocity = [0, 0, 0]

    def get_times(self):
        self.best_time = times.get_best_time("time_trial", logic.game.level_name)

    def write_time(self):
        now = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

        recording_fname = "{}_{}.recording".format(now, logic.game.level_name)
        recording_file_path = os.path.join(
            logic.game.get_profile_dir("0"),
            "time_trial",
            recording_fname
        )
        
        # saves the time to the times file and gets the recording urn
        urn = times.save_time("time_trial", self.final_time, now, recording_fname)
        
        # saves the recording to to a file
        recording = {
            "urn": urn,
            "coords": [(v[0], list(v[1]), list([list(w) for w in v[2]])) for v in logic.recording]
        }
        with open(recording_file_path, "w") as f:
            json.dump(recording, f)

    def countdown(self):
        if not "countdown" in self.go:
            self.go["countdown"] = 4
        if self.go["countdown"] < 4:
            self.label_countdown.text = str(self.go["countdown"])
        else:
            self.label_countdown.text = ""
        if self.go["countdown"] == 0:
            self.label_countdown.text = "GO!"
        if self.go["countdown"] == -1:
            self.label_countdown.text = ""
        
        if self.go["countdown"] > -1 and self.go["CountdownTimer"] > 1:
            if self.go["countdown"] == 4:
                sound.play_random("three")
                # sound.EchoWrapper("three0").play()
                self.go["countdown"] -= 1
            if self.go["countdown"] == 3 and self.go["CountdownTimer"] > 2:
                sound.play_random("two")
                self.go["countdown"] -= 1
            if self.go["countdown"] == 2 and self.go["CountdownTimer"] > 3:
                sound.play_random("one")
                self.go["countdown"] -= 1
            if self.go["countdown"] == 1 and self.go["CountdownTimer"] > 4:
                # sound.play_random("go")
                self.label_time.show()
                sound.play_random("go")
                self.go["countdown"] -= 1
                # Give controls to the player
                logic.uim.focus = "ship"
                self.go["Timer"] = 0.0
            if self.go["countdown"] == 0 and self.go["CountdownTimer"] > 5:
                self.go["countdown"] -= 1

    def restore_camera(self):
        sce = get_scene("Scene")
        if logic.settings["Player"]["camera"] == '1':
            sce.active_camera = sce.objects["Camera_Ship"]
            logic.game.ships[0].go.children["Mesh"].visible = True

        if logic.settings["Player"]["camera"] == '2':
            sce.active_camera = sce.objects["camera_ship_hood"]
            logic.game.ships[0].go.children["Mesh"].visible = True

        if logic.settings["Player"]["camera"] == '3':
            sce.active_camera = sce.objects["camera_ship_front"]
            logic.game.ships[0].go.children["Mesh"].visible = False

    def checkpoints(self):
        sce = logic.getCurrentScene()
        for cp in self.cp_data:
            for ship in logic.game.ships:
                if logic.game.ships[ship].go.getDistanceTo(cp) >= trigger_distance:
                    continue
                if cp["0"]:
                    continue

                cp[str(ship)] = True
                sound.play("checkpoint")
                if G.DEBUG:
                    print("Ship", ship, "passed", self.cp_data.index(cp))

                if "force_camera" in cp:
                    if G.DEBUG: print("FORCE CAM")
                    if cp["force_camera"] == 0:
                        sce.active_camera = sce.objects["Camera_Ship"]
                        logic.game.ships[ship].go.children["Mesh"].visible = True
                    elif cp["force_camera"] == 1 and not logic.settings["Player"]["camera"] == '3':
                        sce.active_camera = sce.objects["camera_ship_hood"]
                        logic.game.ships[ship].go.children["Mesh"].visible = True
                    elif cp["force_camera"] == 2 and not logic.settings["Player"]["camera"] == '2':
                        sce.active_camera = sce.objects["camera_ship_front"]
                        logic.game.ships[ship].go.children["Mesh"].visible = False
                
                elif "restore_camera" in cp:
                    if G.DEBUG: print("RESTORE CAM")
                    self.restore_camera()

                cp.color = [.05, .05, .0, 1]
                cp.worldScale = [0.2, 0.2, 0.2]

                amnt_passed = 0
                for cp in self.cp_data:
                    if cp["0"]:
                        amnt_passed += 1
                        self.split_times.append(self.go["Timer"])
                        
                if G.DEBUG: print(amnt_passed, "/", len(self.cp_data))
                self.cp_progress[str(ship)] = amnt_passed
                
                if amnt_passed == len(self.cp_data) - 1:
                    sound.play_random("last_checkpoint")
                
                # checks if the time trial is complete
                if amnt_passed == len(self.cp_data):

                    # shows the menu
                    menu = logic.ui["time_trial"].get_element("pause_menu")
                    menu.show()
                    menu.focus()
                    logic.uim.set_focus("menu")
                    self.go["ui_timer"] = 0

                    # ends the time trial and saves the final time
                    self.mode_done = True
                    self.final_time = self.go["Timer"]

                    if self.final_time < self.best_time["time"]:
                        sound.play_random("new_record")
                    else:
                        sound.play_random("announcer_complete")

                    # records one last frame
                    logic.recording.append((self.final_time, logic.game.ships[ship].go.worldPosition.copy(), logic.game.ships[ship].go.worldOrientation.copy()))

                    if G.DEBUG: print("Time Trial over.")

                    # saves the time and recoridng to files
                    self.write_time()

                    sound.play("race_complete")


def update_label_speed(widget):
    ship = logic.game.get_ship_by_player(0)
    if ship:
        # self.bar_boost.percent = ship.current_boost/500
        widget.text = ">>> " + str(int(ship.current_velocity))

def update_label_time(widget):
    own = logic.time_trial.go
    tt = logic.time_trial
    if own["CountdownTimer"] > 4:
        if not tt.cp_count == tt.cp_progress["0"]:
            widget.text = helpers.time_string(own["Timer"])


def update_label_best(widget):
    tt = logic.time_trial

    if tt.best_time["player"] == "":
        widget.text = "NO BEST TIME YET"
    else:
        widget.text  = "BEST: {} ({})".format(
            helpers.time_string(tt.best_time["time"]),
            tt.best_time["player"]
        )

def update_label_split(widget):
    tt = logic.time_trial
    
    if not tt.split_times:
        widget.text = ""
        return
        
    if tt.split_times[-1] + 2.0 > tt.go["Timer"]:
        if tt.cp_progress["0"] == len(tt.cp_data):
            if tt.final_time < tt.best_time["time"]:
                widget.text = "BEST TIME!"
            else:
                widget.text = "[ +{} ]".format(time_string(tt.final_time - tt.best_time["time"]))
        else:
            widget.text = time_string(tt.split_times[-1])
        widget.go.color = [1., 1., 1., tt.split_times[-1] + 2.0 - tt.go["Timer"]]
    else:
        widget.text = ""


def update_label_checkpoints(widget):
    tt = logic.time_trial
    widget.text = "Chk " + str(tt.cp_progress[str(0)]) +"/"+ str(tt.cp_count)


def update_checkpoint_bar(widget):
    tt = logic.time_trial
    if tt.cp_count != 0:
        widget.progress = tt.cp_progress[str(0)] / tt.cp_count


def update_boost_bar(widget):
    ship = logic.game.get_ship_by_player(0)
    if ship:
        widget.progress = ship.current_boost/500


def init():
    """ Runs immediately after the scene loaded """
    logic.time_trial = Time_Trial_Mode(logic.getCurrentController().owner)
    logic.recording = []


    # Creates a folder for the mode
    if not os.path.isdir(os.path.join(logic.game.get_profile_dir("0"),"time_trial")):
        os.makedirs(os.path.join(logic.game.get_profile_dir("0"),"time_trial"))


def record():
    if logic.time_trial.go["CountdownTimer"] > 4 and not logic.time_trial.mode_done:
        ship = helpers.get_scene("Scene").objects["Ship"]
        logic.recording.append((logic.time_trial.go["Timer"], ship.worldPosition.copy(), ship.worldOrientation.copy()))

    
def main():
    logic.time_trial.run()
