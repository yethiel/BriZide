import os, json
from bge import logic, render
from mathutils import Matrix
from modules import btk, times, global_constants as G
from modules.helpers import get_scene, time_string, keystat
from modules.game_mode import Game_Mode

required_components = ["blocks", "level", "cube"]


class Replay_Mode(Game_Mode):
    def __init__(self, game_obj):
        # initiates the game mode, name needs to match folder name
        super().__init__(required_components, game_obj, "replay")
        self.recording = None  # recording data
        self.init = False  # whether the game mode has been set up
        self.current_frame = 0  # current frame of the replay
        self.paused = False  # whether the replay is paused
        self.paused_time = 0.0  # time at which the replay has been paused

    def setup(self):
        """ runs after loading is done """
        super().setup()
        logic.game.set_music_dir("ambient")
        logic.uim.set_focus("ship")

        # Layout created by the game mode
        self.layout = logic.ui["replay"]

        # bar for replay progress
        self.progress_bar = btk.ProgressBar(
            self.layout,
            title="progress_bar",
            position=[0.5, 0.45, -0.1],
            min_scale=[0, .4, 1],
            max_scale=[15, .4, 1],
            update=None
        )
        self.progress_bar.set_color([1, .743, 0.0, 0.75])
        
        # labels for the progress bar
        self.play_status = btk.Label(self.layout, text=">", position=[0.7, 0.53, 0], size=0.4, update=None)
        self.progress_bar_label = btk.Label(self.layout, text="", position=[1.5, 0.5, 0], size=0.4, update=None)
        
        # replay information labels
        btk.Label(self.layout, text="TIME: {}".format(time_string(self.replay_run["time"])), position=[0.5, 8.0, 0], size=0.28)
        btk.Label(self.layout, text="LEVEL: {}".format(logic.game.level_name), position=[0.5, 7.7, 0], size=0.28)
        btk.Label(self.layout, text="PLAYER: {}".format(self.replay_run["player"]), position=[0.5, 7.4, 0], size=0.28)
        btk.Label(self.layout, text="SHIP: {}".format(self.replay_run["ship"]), position=[0.5, 7.1, 0], size=0.28)
        btk.Label(self.layout, text="DATE: {}".format(self.replay_run["date"]), position=[0.5, 6.8, 0], size=0.28)

        # replay control labels
        btk.Label(self.layout, text="[ left ]  <<", position=[0.5, 1.0, 0], size=0.3)
        btk.Label(self.layout, text="[ right ]  >>", position=[2.3, 1.0, 0], size=0.3)
        btk.Label(self.layout, text="[ up ]  I<", position=[4.3, 1.0, 0], size=0.3)
        btk.Label(self.layout, text="[ down ]  >I", position=[5.9, 1.0, 0], size=0.3)
        btk.Label(self.layout, text="[ space ]  II", position=[8.0, 1.0, 0], size=0.3)

        self.init = True


    def run(self):
        """ runs every logic tick """
        super().run()  # handles loading of components

        if not self.init:
            return

        sce = get_scene("Scene")

        cam = sce.active_camera = sce.objects["camera_replay"]

        if not self.recording:
            return

        if not self.paused:
            self.play_status.text = ">"
        else:
            self.play_status.text = "II"

        # fast forward
        if keystat("LEFTARROWKEY", "ACTIVE") and self.current_frame > 0:
            self.current_frame -= 1
            self.go["Timer"] = self.recording["coords"][self.current_frame][0]
            self.play_status.text = "<<"
            if not self.paused:
                logic.game.go["radial"] = 0.2
            else:
                self.paused_time = self.recording["coords"][self.current_frame][0]

        # backward
        elif keystat("RIGHTARROWKEY", "ACTIVE") and self.current_frame < len(self.recording["coords"])-1:
            self.current_frame += 1
            self.go["Timer"] = self.recording["coords"][self.current_frame][0]
            self.play_status.text = ">>"
            if not self.paused:
                logic.game.go["radial"] = 0.2
            else:
                self.paused_time = self.recording["coords"][self.current_frame][0]
        else:
            logic.game.go["radial"] = 0.0

        # jumps to the beginning
        if keystat("UPARROWKEY", "JUST_RELEASED"):
            self.current_frame = 0
            self.go["Timer"] = 0

        # jumps to the end
        elif keystat("DOWNARROWKEY", "JUST_RELEASED"):
            self.current_frame = len(self.recording["coords"]) - 1
            self.go["Timer"] = self.recording["coords"][-1][0]

        # toggles play and pause
        if keystat("SPACEKEY", "JUST_RELEASED"):
            if not self.paused:
                self.paused_time = self.go["Timer"]
            else:
                self.go["Timer"] = self.paused_time
            self.paused = not self.paused
            self.play_status.text = "II"

        ghost = sce.objects["Ghost"]

        time = self.recording["coords"][self.current_frame][0]
        total_time = self.recording["coords"][-1][0]
        self.progress_bar.progress = time / total_time
        self.progress_bar_label.text = "{} / {}".format(time_string(time), time_string(total_time))

        pos = self.recording["coords"][self.current_frame][1]
        orientation = self.recording["coords"][self.current_frame][2]
        ghost.worldPosition = pos
        ghost.worldOrientation = Matrix(orientation)
        print(time, self.go["Timer"])
        if not self.paused and self.go["Timer"] > time and self.current_frame < len(self.recording["coords"])-1:
            if G.DEBUG: print(time, self.go["Timer"])
            self.current_frame += 1


def init():
    """ Runs immediately after the scene loaded """
    logic.replay_mode = Replay_Mode(logic.getCurrentController().owner)
    runs = times.load_times("time_trial", logic.game.level_name)
    logic.replay_mode.replay_run = runs[logic.game.replay_urn]
    recording_fname = logic.replay_mode.replay_run["recording"]
    recording_path = os.path.join(G.PATH_PROFILES, logic.replay_mode.replay_run["player"], "time_trial", recording_fname)

    with open(recording_path, "r") as f:
        recording = json.load(f)

    logic.replay_mode.recording = recording

    print(recording)


def main():
    logic.replay_mode.run()
